FROM node:10.19.0-alpine3.9
ENV NODE_ENV=development
ENV DEBUG=axios
WORKDIR /usr/src/app
COPY package*.json ./
RUN yarn install
COPY . .
EXPOSE 3011
# RUN [ "npm", "run", "build" ]
CMD [ "yarn", "start:prod" ]
