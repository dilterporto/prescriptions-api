# prescriptions-api

A solução consiste em uma API REST [POST] /v2/prescriptions para inserir novas prescrições.

## PR's com o progresso do desenvolvimento

https://github.com/dilterporto/prescriptions-api/pull/1
---
https://github.com/dilterporto/prescriptions-api/pull/2
---
https://github.com/dilterporto/prescriptions-api/pull/3
---


## Execução da app com Docker

```
docker-compose up
```

## Acesso à documentação da API

```
http://localhost:3015/docs
```

## Execução dos testes

```
yarn test
```

