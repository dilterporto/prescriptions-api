import { app } from './app';
import constants from './config/constants';
import * as chalk from 'chalk';

app.listen(constants.port, () => {
  console.log(chalk.greenBright(`✓ Started API server at port ${constants.port}`));
});
