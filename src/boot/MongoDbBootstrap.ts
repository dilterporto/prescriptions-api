import * as mongoose from 'mongoose';
import * as chalk from 'chalk';
import constants from '../config/constants';
import { IBootstrap } from './Bootstrap';

export class MongoDbBoostrap implements IBootstrap {
  public async setup (): Promise<void> {
    mongoose.set('debug', true);
    try {
      await mongoose.connect(constants.mongodbConnectionString, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      });
      console.log(chalk.greenBright(`✓ MongoDB Database connected at ${constants.mongodbConnectionString}`));
    } catch (err) {
      console.log(chalk.white.bgRed(`Error: ${err.message}`));
    }
  }
}
