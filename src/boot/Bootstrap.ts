import { Express } from 'express';

export interface IBootstrap {
  setup(app: Express): Promise<void>;
}
