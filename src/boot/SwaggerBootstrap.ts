import * as chalk from 'chalk';
import * as swaggerUi from 'swagger-ui-express';
import { Express } from 'express';
import { IBootstrap } from './Bootstrap';

export class SwaggerBootstrap implements IBootstrap {
  public async setup (app: Express): Promise<void> {
    try {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const swaggerDocument = require('../../build/swagger/swagger.json');
      app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    } catch (err) {
      console.log(chalk.white.bgRed(`Error generating api docs: ${err.message}`));
    }
  }
}
