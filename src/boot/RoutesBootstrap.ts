import * as chalk from 'chalk';
import { Express } from 'express';
import { RegisterRoutes } from '../../build/routes';
import { IBootstrap } from './Bootstrap';

export class RoutesBootstrap implements IBootstrap {
  public async setup (app: Express): Promise<void> {
    try {
      RegisterRoutes(app);
    } catch (err) {
      console.log(chalk.white.bgRed(`Error: ${err.message}`));
    }
  }
}
