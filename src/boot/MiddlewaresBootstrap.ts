import { Express } from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import { IBootstrap } from './Bootstrap';
import { Logger } from '../config/Logger';

export class MiddlewaresBootstrap implements IBootstrap {
  public async setup (app: Express): Promise<void> {
    app.use(cors());
    app.use(bodyParser.json());
    app.disable('x-powered-by');

    const unhandledErrorHandler = (...args: any[]): void => {
      Logger.error('Unhandled Error...', ...args);
      process.exit(1);
    };
    process.on('uncaughtException', unhandledErrorHandler);
    process.on('unhandledRejection', unhandledErrorHandler);
  }
}
