import { AddSingleton } from '../core/ioc';
import { EventEmitter } from 'events';

@AddSingleton(Bus)
export class Bus {
  public readonly emitter: EventEmitter;
  constructor () {
    this.emitter = new EventEmitter();
  }

  public async send (request: any): Promise<void> {
    console.log(request.constructor.name, request);
    this.emitter.emit(request.constructor.name, request);
  }
}
