import * as axios from 'axios';
import * as cachios from 'cachios';
import * as AxiosLogger from 'axios-logger';
import { Policy, RetryPolicy, TimeoutPolicy, TimeoutStrategy } from 'cockatiel';
import constants from '../config/constants';
import { Logger } from '../config/Logger';
import { HttpError, HttpNotFountError, IResponse } from './response';

/**
 * generic http client
 */
export default class ApiClient {
  protected instance: any;
  private retry: RetryPolicy;
  private timeout: TimeoutPolicy;
  private ttl: number;

  constructor (client: string) {
    const { clients } = constants;
    const config = clients[client];
    const axiosInstance = axios.create({
      baseURL: config.url,
      headers: {
        Authorization: `Bearer ${config.auth.token}`
      }
    });
    axiosInstance.interceptors.request.use(AxiosLogger.requestLogger);
    this.instance = cachios.create(axiosInstance);
    this.applyPolicies(config);
  }

  public async post<TData> (path: string, data: TData): Promise<void | HttpError> {
    try {
      await Policy
        .wrap(this.retry, this.timeout)
        .execute(async () => {
          await this.instance.post(path, data);
        });
    } catch (err) {
      const { response } = err;
      const { status, statusText: message } = response;
      const errorResponse: HttpError = {
        status,
        message
      };
      return errorResponse;
    }
  }

  public async get<TData> (path: string): Promise<IResponse<TData> | HttpNotFountError | HttpError> {
    try {
      const result = await Policy
        .wrap(this.retry, this.timeout)
        .execute(async () => {
          return (await this.instance.get(path, { ttl: this.ttl })).data;
        });
      const { data } = result;
      const response: IResponse<TData> = {
        data
      };
      return response;
    } catch (err) {
      const { response } = err;
      if (response.status === 404) {
        return new HttpNotFountError(response.statusText);
      } else {
        const { status, statusText: message } = response;
        const errorResponse: HttpError = {
          status,
          message
        };
        return errorResponse;
      }
    }
  }

  private async applyPolicies (config: any): Promise<void> {
    this.retry = Policy
      .handleAll()
      .retry()
      .attempts(config.policy.retry);
    this.timeout = Policy.timeout(config.timeout, TimeoutStrategy.Aggressive);
    this.ttl = config.cache ? config.cache.ttl : 0;
  }
}
