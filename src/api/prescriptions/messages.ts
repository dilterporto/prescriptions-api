interface Clinic {
  id: number;
}

interface Physician {
  id: number;
}

interface Patient {
  id: number;
}

class Prescription {
  clinic: Clinic;
  physician: Physician;
  patient: Patient;
  text: string;
}

export class PrescriptionRequest extends Prescription {

}

export class PrescriptionResponse {
  data: Prescription;
}
