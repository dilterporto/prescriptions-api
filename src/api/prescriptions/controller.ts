import { Body, Controller, Post, Route, Tags, SuccessResponse, Response } from 'tsoa';
import { inject, AddSingleton } from '../../core/ioc';
import { PrescriptionRequest, PrescriptionResponse } from './messages';
import ApiError from '../../core/apiError';
import PrescriptionsRequestHandler from './handlers';

/**
 * Prescriptions controller
 */
@Tags('prescriptions')
@Route('v2/prescriptions')
@AddSingleton(PrescriptionsController)
export class PrescriptionsController extends Controller {
  constructor (@inject(PrescriptionsRequestHandler) private handler: PrescriptionsRequestHandler) {
    super();
  }

  /**
   * creates an prescription
   * @param body
   */
  @SuccessResponse('201', 'Prescription Created Successfuly')
  @Response<ApiError>('422', 'Malformed Request')
  @Response<ApiError>('422', 'Physician Not Found')
  @Response<ApiError>('422', 'Patient Not Found')
  @Response<ApiError>('422', 'Metrics Services Not Available')
  @Response<ApiError>('422', 'Physicians Services Not Available')
  @Response<ApiError>('422', 'Patients Services Not Available')
  @Post()
  public async createPrescription (@Body() body: PrescriptionRequest): Promise<PrescriptionResponse | ApiError[]> {
    try {
      const response = await this.handler.handlePrescriptionRequest(body);
      const prescriptionCreated = !Array.isArray(response);
      if (prescriptionCreated) {
        this.setStatus(201);
        return response;
      }
      this.setStatus(422);
      return response;
    } catch (err) {
      this.setStatus(500);
      const errors: ApiError[] = [
        {
          error: {
            code: 0,
            message: err.message
          }
        }
      ];
      return errors;
    }
  }
}
