import { PrescriptionRequest, PrescriptionResponse } from './messages';
import { AddSingleton, inject } from '../../core/ioc';
import ApiError from '../../core/apiError';
import Prescription from '../../models/Prescription';
import ClinicApiClient, { ClinicResource } from '../../clients/ClinicApiClient';
import PatientApiClient, { PatientResource } from '../../clients/PatientApiClient';
import PhysicianApiClient, { PhysicianResource } from '../../clients/PhysicianApiClient';
import MetricsApiClient, { MetricsRequest } from '../../clients/MetricsApiClient';
import { PatientNotFoundError, PhysicianNotFoundError, PhysiciansServicesNotAvailableError, MetricsServicesNotAvailableError } from '../../clients/errors';

/**
 * handles prescription endpoints
 */
@AddSingleton(PrescriptionsRequestHandler)
export default class PrescriptionsRequestHandler {
  constructor (
    @inject(ClinicApiClient) private clinicApiClient: ClinicApiClient,
    @inject(PatientApiClient) private patientApiClient: PatientApiClient,
    @inject(PhysicianApiClient) private physicianApiClient: PhysicianApiClient,
    @inject(MetricsApiClient) private metricsApiClient: MetricsApiClient) {
  }

  /**
   * handles prescription request
   * @param prescriptionRequest
   */
  public async handlePrescriptionRequest (prescriptionRequest: PrescriptionRequest): Promise<PrescriptionResponse | ApiError[]> {
    const errors: ApiError[] = [];
    const [clinic, patient, physician] = await this.lookupAll(prescriptionRequest);
    if (patient instanceof ApiError) errors.push(patient as ApiError);
    if (physician instanceof ApiError) errors.push(physician as ApiError);

    const createMetrics = await this.sendMetrics(clinic, patient, physician);
    if (createMetrics instanceof ApiError) errors.push(createMetrics);

    if (errors.length) {
      return errors;
    }

    const prescription = new Prescription(prescriptionRequest);
    await prescription.save();
    const response: PrescriptionResponse = {
      data: prescription.toObject()
    };
    return response as PrescriptionResponse;
  }

  private async sendMetrics (clinic: ClinicResource, patient: PatientResource | PatientNotFoundError, physician: PhysicianResource | PhysicianNotFoundError | PhysiciansServicesNotAvailableError): Promise<void | MetricsServicesNotAvailableError> {
    const metrics: MetricsRequest = {
      clinic,
      patient: patient as PatientResource,
      physician: physician as PhysicianResource
    };
    const createMetrics = await this.metricsApiClient.post(metrics);
    return createMetrics;
  }

  private async lookupAll (prescriptionRequest: PrescriptionRequest): Promise<[ClinicResource, PatientResource | PatientNotFoundError, PhysicianResource | PhysicianNotFoundError | PhysiciansServicesNotAvailableError]> {
    const [clinic, patient, physician] = await Promise.all([
      this.clinicApiClient.getById(prescriptionRequest.clinic.id),
      this.patientApiClient.getById(prescriptionRequest.patient.id),
      this.physicianApiClient.getById(prescriptionRequest.physician.id)
    ]);
    return [clinic, patient, physician];
  }
}
