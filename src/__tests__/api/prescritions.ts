import { describe, it } from 'mocha';
import * as request from 'supertest';
import { expect } from 'chai';
import { app } from '../../app';

const validPrescriptionRequest = {
  clinic: {
    id: 1
  },
  physician: {
    id: 1
  },
  patient: {
    id: 1
  },
  text: 'string'
};

const prescriptionRequestWithInvalidClinic = {
  clinic: {
    id: 999 // invalid clinic id
  },
  physician: {
    id: 1
  },
  patient: {
    id: 1
  },
  text: 'string'
};

const prescriptionRequestWithInvalidPhysician = {
  clinic: {
    id: 1
  },
  physician: {
    id: 999 // invalid physician id
  },
  patient: {
    id: 1
  },
  text: 'string'
};

const prescriptionRequestWithInvalidPatient = {
  clinic: {
    id: 1
  },
  physician: {
    id: 1
  },
  patient: {
    id: 999 // invalid patient id
  },
  text: 'string'
};

const prescriptionRequestWithInvalidPatientAndPhysician = {
  clinic: {
    id: 1
  },
  physician: {
    id: 999 // invalid physician id
  },
  patient: {
    id: 999 // invalid patient id
  },
  text: 'string'
};

describe('POST / - prescriptions', () => {
  it('given a valid prescription request should create with 201 status code returned', (done) => {
    request(app)
      .post('/v2/prescriptions')
      .send(validPrescriptionRequest)
      .expect(201)
      .expect((res) => {
        const { body: { data } } = res;
        expect(data).to.have.property('patient');
        expect(data).to.have.property('physician');
        expect(data).to.have.property('text');
        expect(data).to.have.property('clinic');
      })
      .end(done);
  });

  it('given a prescription request with an invalid clinic id should create with 201 status code returned', (done) => {
    request(app)
      .post('/v2/prescriptions')
      .send(prescriptionRequestWithInvalidClinic)
      .expect(201)
      .end(done);
  });

  it('given a prescription request with an invalid physician id should not create with 422 status code returned', (done) => {
    request(app)
      .post('/v2/prescriptions')
      .send(prescriptionRequestWithInvalidPhysician)
      .expect(422)
      .expect((res) => {
        expect(res.body.length).to.equal(1);
        expect(res.body[0].error.message).to.equal('Physician Not Found');
        expect(res.body[0].error.code).to.equal(2);
      })
      .end(done);
  });

  it('given a prescription request with an invalid patient id should not create with 422 status code returned', (done) => {
    request(app)
      .post('/v2/prescriptions')
      .send(prescriptionRequestWithInvalidPatient)
      .expect(422)
      .expect((res) => {
        expect(res.body.length).to.equal(1);
        expect(res.body[0].error.message).to.equal('Patient Not Found');
        expect(res.body[0].error.code).to.equal(3);
      })
      .end(done);
  });

  it('given a prescription request with an invalid patient id and invalid physician id should not create with 422 status code returned', (done) => {
    request(app)
      .post('/v2/prescriptions')
      .send(prescriptionRequestWithInvalidPatientAndPhysician)
      .expect(422)
      .expect((res) => {
        expect(res.body.length).to.equal(2);
      })
      .end(done);
  });

  it('given a prescription request with an invalid patient id and invalid physician id should not create with 422 status code returned', (done) => {
    request(app)
      .post('/v2/prescriptions')
      .send(prescriptionRequestWithInvalidPatientAndPhysician)
      .expect(422)
      .expect((res) => {
        expect(res.body.length).to.equal(2);
      })
      .end(done);
  });
});
