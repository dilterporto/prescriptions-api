import { resolve } from 'path';
import { config } from 'dotenv';

const { env } = process;

config({ path: resolve(__dirname, `./.env/.env.${env.NODE_ENV}`) });

export default {
  environment: env.NODE_ENV,
  port: Number(env.PORT),
  mongodbConnectionString: env.MONGODB_CONNECTIONSTRING,
  clients: {
    clinic: {
      url: env.CLIENT_CLINIC_URL,
      timeout: Number(env.CLIENT_CLINIC_TIMEOUT),
      policy: {
        retry: env.CLIENT_CLINIC_POLICY_RETRY
      },
      auth: {
        token: env.CLIENT_CLINIC_AUTH_TOKEN
      },
      cache: {
        ttl: env.CLIENT_CLINIC_CACHE_TTL
      }
    },
    physician: {
      url: env.CLIENT_PHYSICIAN_URL,
      timeout: Number(env.CLIENT_PHYSICIAN_TIMEOUT),
      policy: {
        retry: env.CLIENT_PHYSICIAN_POLICY_RETRY
      },
      auth: {
        token: env.CLIENT_PHYSICIAN_AUTH_TOKEN
      },
      cache: {
        ttl: env.CLIENT_PHYSICIAN_CACHE_TTL
      }
    },
    patient: {
      url: env.CLIENT_PATIENT_URL,
      timeout: Number(env.CLIENT_PATIENT_TIMEOUT),
      policy: {
        retry: env.CLIENT_PATIENT_POLICY_RETRY
      },
      auth: {
        token: env.CLIENT_PATIENT_AUTH_TOKEN
      },
      cache: {
        ttl: env.CLIENT_PATIENT_CACHE_TTL
      }
    },
    metrics: {
      url: env.CLIENT_METRICS_URL,
      timeout: Number(env.CLIENT_METRICS_TIMEOUT),
      policy: {
        retry: env.CLIENT_METRICS_POLICY_RETRY
      },
      auth: {
        token: env.CLIENT_METRICS_AUTH_TOKEN
      }
    }
  }
};
