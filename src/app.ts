import * as express from 'express';
import { IBootstrap } from './boot/Bootstrap';

import { MiddlewaresBootstrap } from './boot/MiddlewaresBootstrap';
import { SwaggerBootstrap } from './boot/SwaggerBootstrap';
import { RoutesBootstrap } from './boot/RoutesBootstrap';
import { MongoDbBoostrap } from './boot/MongoDbBootstrap';

const app = express();

const boots: IBootstrap[] = [
  new MiddlewaresBootstrap(),
  new RoutesBootstrap(),
  new MongoDbBoostrap(),
  new SwaggerBootstrap()
];

export const setup = async (bootstraps: IBootstrap[]): Promise<void> => {
  bootstraps.forEach(bootstrap => {
    bootstrap.setup(app);
  });
};

setup(boots);

export { app };
