import * as mongoose from 'mongoose';
import { Schema, Document } from 'mongoose';

interface IClinic {
  id: number;
}

interface IPhysician {
  id: number;
}

interface IPatient {
  id: number;
}

export interface IPrescription extends Document {
  text: string;
  patient: IPatient;
  physician: IPhysician;
  clinic: IClinic;
}

const PrescriptionSchema: Schema = new Schema({
  text: String,
  patient: {
    id: Number
  },
  physician: {
    id: Number
  },
  clinic: {
    id: Number
  }
});

export default mongoose.model<IPrescription>('Prescription', PrescriptionSchema);
